# @uxf/select
A set of React hooks to create selects, multi-selects and combo-boxes.

## Quick start

Install package
```
yarn add @uxf/select
```

Install peer dependencies
```
yarn add react react-laag resize-observer-polyfill
```

## API

### useSelect()

#### UseSelectProps

| name         | type                                     | required | default                          |
|--------------|------------------------------------------|----------|----------------------------------|
| defaultValue | `number\|string`                         |          |                                  |
| disabled     | `boolean`                                |          |                                  |
| isMobile     | `boolean`                                |          |                                  |
| isApple      | `boolean`                                |          |                                  |
| itemToString | `(item?: Option\|null) => string`        |          | `item => item?.label \|\| ""` |
| name         | `string`                                 | yes      |                                  |
| onChange     | `(value: Option\|null) => void`          | yes      |                                  |
| options      | `Option[]`                               | yes      |                                  |
| value        | `number\|string`                         |          |                                  |

### useMultiSelect()

#### UseMultiSelectProps

| name         | type                                     | required | default                          |
|--------------|------------------------------------------|----------|----------------------------------|
| defaultValue | `(number\|string)[]`                     |          |                                  |
| disabled     | `boolean`                                |          |                                  |
| isMobile     | `boolean`                                |          |                                  |
| isApple      | `boolean`                                |          |                                  |
| itemToString | `(item?: Option\|null) => string`        |          | `item => item?.label \|\| ""`    |
| name         | `string`                                 | yes      |                                  |
| onChange     | `(value: Option[]\|null) => void`        | yes      |                                  |
| options      | `Option[]`                               | yes      |                                  |
| value        | `(number\|string)[]`                     |          |                                  |

### useCombobox()

#### UseComboboxProps

| name                    | type                                                      | required | default                          |
|-------------------------|-----------------------------------------------------------|----------|----------------------------------|
| allowDeselect           | `boolean`                                                 |          | `true`                           |
| alwaysVisibleOptionsIds | `(number\|string)[]`                                      |          |                                  |
| defaultValue            | `(number\|string)`                                        |          |                                  |
| disabled                | `boolean`                                                 |          |                                  |
| isMobile                | `boolean`                                                 |          |                                  |
| isApple                 | `boolean`                                                 |          |                                  |
| itemToString            | `(item?: Option\|null) => string`                         |          | `item => item?.label \|\| ""`    |
| loadOptions             | `(value?: string) => Promise<Option[]\|undefined>`        |          |                                  |
| name                    | `string`                                                  | yes      |                                  |
| onChange                | `(value:  Option\|null) => void`                          | yes      |                                  |
| options                 | `Option[]`                                                |          |                                  |
| value                   | `(number\|string)`                                        |          |                                  |

### useMultiCombobox()

#### UseMultiComboboxProps

| name                    | type                                                      | required | default                          |
|-------------------------|-----------------------------------------------------------|----------|----------------------------------|
| alwaysVisibleOptionsIds | `(number\|string)[]`                                      |          |                                  |
| defaultValue            | `(number\|string)[]`                                      |          |                                  |
| disabled                | `boolean`                                                 |          |                                  |
| isMobile                | `boolean`                                                 |          |                                  |
| isApple                 | `boolean`                                                 |          |                                  |
| itemToString            | `(item?: Option\|null) => string`                         |          | `item => item?.label \|\| ""`    |
| loadOptions             | `(value?: string) => Promise<Option[]\|undefined>`        |          |                                  |
| name                    | `string`                                                  | yes      |                                  |
| onChange                | `(value: Option[]\|null) => void`                         | yes      |                                  |
| options                 | `Option[]`                                                |          |                                  |
| value                   | `(number\|string)[]`                                      |          |                                  |
