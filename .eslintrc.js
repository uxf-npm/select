// eslint-disable-next-line no-undef
module.exports = {
    parser: "@typescript-eslint/parser", // Specifies the ESLint parser
    extends: [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended", // Uses the recommended rules from @typescript-eslint/eslint-plugin
        "plugin:prettier/recommended", // Enables eslint-plugin-prettier and displays prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array
    ],
    parserOptions: {
        ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
        sourceType: "module", // Allows for the use of imports
        ecmaFeatures: {
            jsx: true, // Allows for the parsing of JSX
        },
        project: "./tsconfig.json",
    },
    plugins: ["react-hooks"],
    rules: {
        "@typescript-eslint/interface-name-prefix": "off",
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/no-explicit-any": "off",

        // new rules
        "@typescript-eslint/no-unused-vars": "error",
        "no-console": "error",
        "no-await-in-loop": "error",
        "no-dupe-else-if": "error",
        "no-import-assign": "error",
        "no-setter-return": "error",
        "no-template-curly-in-string": "error",
        "require-atomic-updates": "error",
        "block-scoped-var": "error",
        complexity: "error",
        curly: "error",
        "dot-notation": "error",
        eqeqeq: "error",
        "max-classes-per-file": ["error", 1],
        "no-alert": "error",
        "no-caller": "error",
        "no-constructor-return": "error",
        "no-div-regex": "error",
        "no-else-return": "error",
        "no-empty-function": "error",
        "no-eq-null": "error",
        "no-eval": "error",
        "no-extra-bind": "error",
        "no-extra-label": "error",
        "no-useless-return": "error",
        "no-unused-expressions": ["error", { allowTernary: true }],
        radix: "error",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "no-use-before-define": "off",
        "@typescript-eslint/no-use-before-define": "error",
        "no-shadow": "off",
        "@typescript-eslint/no-shadow": "error",
        "@typescript-eslint/ban-types": "error",
        "react-hooks/exhaustive-deps": "error",
        "react-hooks/rules-of-hooks": "error",
        "react/react-in-jsx-scope": "off",
        "react/prop-types": "off",
        "@typescript-eslint/no-unnecessary-condition": "error",
    },
    settings: {
        react: {
            version: "detect", // Tells eslint-plugin-react to automatically detect the version of React to use
        },
    },
    globals: {
        css: true,
    },
    ignorePatterns: ["dist/", "demo/"],
};
