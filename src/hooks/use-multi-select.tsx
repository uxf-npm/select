import { useMultipleSelection, useSelect, UseSelectGetMenuPropsOptions } from "downshift";
import React, { HTMLAttributes, MouseEvent as ReactMouseEvent, useCallback } from "react";
import { useLayer } from "react-laag";
import { useGetLayerOptions } from "../utils/use-get-layer-options";

export type MultiSelectOption<T extends any = number | string> = {
    key?: number | string;
    id: T;
    label: string;
    disabled?: boolean;
};

export type UseMultiSelectProps<Option extends MultiSelectOption> = {
    defaultValue?: Option["id"][];
    disabled?: boolean;
    isMobile?: boolean;
    isApple?: boolean;
    itemToString?: (item?: Option | null) => string;
    name: string;
    onChange: (value: Option[] | null) => void;
    options: Option[];
    value?: Option["id"][];
};

export const useMultiSelect = <Option extends MultiSelectOption>(props: UseMultiSelectProps<Option>) => {
    const {
        defaultValue,
        disabled,
        isMobile,
        isApple,
        itemToString = item => item?.label || "",
        name,
        onChange,
        options,
        value,
    } = props;

    const layerDefaultOptions = useGetLayerOptions(isApple && isMobile);

    const { getSelectedItemProps, getDropdownProps, addSelectedItem, removeSelectedItem, selectedItems } =
        useMultipleSelection<Option>({
            initialSelectedItems: options.filter(val => value?.includes(val.id) ?? defaultValue?.includes(val.id)),
            onSelectedItemsChange: changes => {
                onChange(changes.selectedItems ?? []);
            },
        });

    const { getItemProps, getMenuProps, getToggleButtonProps, highlightedIndex, isOpen, selectItem } =
        useSelect<Option | null>({
            id: name,
            items: options,
            itemToString,
            onSelectedItemChange: changes => {
                if (changes.selectedItem && !selectedItems.includes(changes.selectedItem)) {
                    addSelectedItem(changes.selectedItem);
                }
            },
        });

    const _removeSelectedItem = useCallback(
        (item: Option) => {
            removeSelectedItem(item);
            selectItem(null);
        },
        [removeSelectedItem, selectItem],
    );

    const { renderLayer, layerProps, layerSide, triggerProps, triggerBounds } = useLayer({
        ...layerDefaultOptions,
        isOpen,
    });

    const getListProps = ({ style, ...rest }: UseSelectGetMenuPropsOptions) =>
        getMenuProps({
            ...layerProps,
            style: { width: triggerBounds?.width, ...layerProps.style, ...(style ?? {}) },
            ...rest,
        });

    const getInputContainerProps = (triggerOptions?: HTMLAttributes<HTMLElement>) =>
        getToggleButtonProps({
            ...triggerProps,
            ...triggerOptions,
            disabled: disabled,
            onClick: event => {
                (
                    event as ReactMouseEvent<HTMLButtonElement, MouseEvent> & {
                        preventDownshiftDefault?: boolean;
                    }
                ).preventDownshiftDefault = isOpen;
            },
        });

    const renderList = isOpen ? renderLayer : () => <span hidden ref={getMenuProps().ref} />;

    return {
        getDropdownProps,
        getInputContainerProps,
        getItemProps,
        getListProps,
        getSelectedItemProps,
        highlightedIndex,
        isOpen,
        layerSide,
        removeSelectedItem: _removeSelectedItem,
        renderList,
        selectedItems,
    };
};
