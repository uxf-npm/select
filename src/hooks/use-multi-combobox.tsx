import { useCombobox, UseComboboxGetMenuPropsOptions, useMultipleSelection } from "downshift";
import React, { HTMLAttributes, MouseEvent as ReactMouseEvent, useCallback, useRef, useState } from "react";
import { useLayer } from "react-laag";
import { filterOptions } from "../utils/filter-options";
import { useGetLayerOptions } from "../utils/use-get-layer-options";

export type MultiComboboxOption<T extends any = number | string> = {
    key?: number | string;
    id: T;
    label: string;
    disabled?: boolean;
};

type AsyncOrNot<Option extends MultiComboboxOption> =
    | {
          loadOptions: (value?: string) => Promise<Option[] | undefined>;
          options?: never;
      }
    | {
          loadOptions?: never;
          options: Option[];
      };

export type UseMultiComboboxProps<Option extends MultiComboboxOption> = {
    alwaysVisibleOptionsIds?: Option["id"][];
    defaultValue?: Option["id"][];
    disabled?: boolean;
    isMobile?: boolean;
    isApple?: boolean;
    itemToString?: (item?: Option | null) => string;
    name: string;
    onChange: (value: Option[] | null) => void;
    value?: Option["id"][];
} & AsyncOrNot<Option>;

export const useMultiCombobox = <Option extends MultiComboboxOption>(props: UseMultiComboboxProps<Option>) => {
    const {
        alwaysVisibleOptionsIds,
        defaultValue,
        disabled,
        isMobile,
        isApple,
        itemToString = item => item?.label || "",
        loadOptions,
        name,
        onChange,
        options,
        value,
    } = props;

    const isAsync = !options && !!loadOptions;
    const layerDefaultOptions = useGetLayerOptions(isApple && isMobile);
    const [inputItems, setInputItems] = useState(options ?? []);
    const [isLoading, setIsLoading] = useState(false);
    const asyncLoadingCounter = useRef(0);

    const handleAsyncLoading = useCallback(
        async (loadingVersion: number, val?: string): Promise<[Option[], number]> => {
            let data: Option[] = [];
            if (loadOptions && val) {
                data = (await loadOptions(val)) ?? [];
            }

            return [data, loadingVersion];
        },
        [loadOptions],
    );

    const { getSelectedItemProps, getDropdownProps, addSelectedItem, removeSelectedItem, selectedItems } =
        useMultipleSelection({
            initialSelectedItems: !isAsync
                ? options?.filter(val => value?.includes(val.id) ?? defaultValue?.includes(val.id))
                : inputItems.filter(i => value?.includes(i.id) ?? defaultValue?.includes(i.id)),
            onSelectedItemsChange: changes => {
                onChange(changes.selectedItems ?? []);
                setInputItems((options ?? []).filter(o => !changes.selectedItems?.includes(o)));
            },
        });

    const {
        getComboboxProps,
        getInputProps,
        getItemProps,
        getMenuProps,
        getToggleButtonProps,
        highlightedIndex,
        isOpen,
        selectItem,
        setInputValue,
    } = useCombobox<Option | null>({
        id: name,
        items: inputItems,
        onInputValueChange: async changes => {
            if (isAsync) {
                setIsLoading(true);
                asyncLoadingCounter.current++;
                const [loadedOptions, loadingVersion] = await handleAsyncLoading(
                    asyncLoadingCounter.current,
                    changes.inputValue,
                );
                if (asyncLoadingCounter.current === loadingVersion) {
                    setInputItems(loadedOptions.filter(o => !value?.includes(o.id)));
                    setIsLoading(false);
                }
            } else {
                if (changes.inputValue?.length) {
                    setInputItems(
                        filterOptions(changes.inputValue, options, alwaysVisibleOptionsIds).filter(
                            o => !value?.includes(o.id),
                        ),
                    );
                } else {
                    setInputItems((options ?? []).filter(o => !value?.includes(o.id)));
                }
            }
        },
        onStateChange: changes => {
            if (changes.type === useCombobox.stateChangeTypes.ToggleButtonClick) {
                setInputItems((options ?? []).filter(o => !value?.includes(o.id)));
            } else if (
                changes.type === useCombobox.stateChangeTypes.InputKeyDownEnter ||
                changes.type === useCombobox.stateChangeTypes.ItemClick ||
                changes.type === useCombobox.stateChangeTypes.InputBlur
            ) {
                if (changes.selectedItem) {
                    setInputValue("");
                    if (!selectedItems.find(i => i.id === changes.selectedItem?.id)) {
                        addSelectedItem(changes.selectedItem);
                    }
                    selectItem(null);
                }
            }
        },
        itemToString,
    });

    const { renderLayer, layerProps, layerSide, triggerProps, triggerBounds } = useLayer({
        ...layerDefaultOptions,
        isOpen,
    });

    const getListProps = ({ style, ...rest }: UseComboboxGetMenuPropsOptions) =>
        getMenuProps({
            ...layerProps,
            style: { width: triggerBounds?.width, ...layerProps.style, ...(style ?? {}) },
            ...rest,
        });

    const getInputContainerProps = (triggerOptions?: HTMLAttributes<HTMLElement>) =>
        getToggleButtonProps({
            ...triggerProps,
            ...triggerOptions,
            disabled: disabled,
            onClick: event => {
                (
                    event as ReactMouseEvent<HTMLButtonElement, MouseEvent> & {
                        preventDownshiftDefault?: boolean;
                    }
                ).preventDownshiftDefault = isOpen;
            },
        });

    const renderList = isOpen ? renderLayer : () => <span hidden ref={getMenuProps().ref} />;

    return {
        getComboboxProps,
        getDropdownProps,
        getInputContainerProps,
        getInputProps,
        getItemProps,
        getListProps,
        getSelectedItemProps,
        highlightedIndex,
        inputItems,
        isLoading,
        isOpen,
        layerSide,
        removeSelectedItem,
        renderList,
        selectedItems,
    };
};
