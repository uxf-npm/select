import { useCombobox as useDownshiftCombobox, UseComboboxGetMenuPropsOptions } from "downshift";
import React, { HTMLAttributes, MouseEvent as ReactMouseEvent, useCallback, useRef, useState } from "react";
import { useLayer } from "react-laag";
import { filterOptions } from "../utils/filter-options";
import { useGetLayerOptions } from "../utils/use-get-layer-options";

export type ComboboxOption<T extends any = number | string> = {
    key?: number | string;
    id: T;
    label: string;
    disabled?: boolean;
};

type AsyncOrNot<Option extends ComboboxOption> =
    | {
          loadOptions: (value?: string) => Promise<Option[] | undefined>;
          options?: never;
      }
    | {
          loadOptions?: never;
          options: Option[];
      };

export type UseComboboxProps<Option extends ComboboxOption> = {
    allowDeselect?: boolean;
    alwaysVisibleOptionsIds?: Option["id"][];
    defaultValue?: any;
    disabled?: boolean;
    isMobile?: boolean;
    isApple?: boolean;
    itemToString?: (item?: Option | null) => string;
    name: string;
    onChange: (value: Option | null) => void;
    value?: any;
} & AsyncOrNot<Option>;

export const useCombobox = <Option extends ComboboxOption>(props: UseComboboxProps<Option>) => {
    const {
        allowDeselect = true,
        alwaysVisibleOptionsIds,
        defaultValue,
        disabled,
        isMobile,
        isApple,
        itemToString = item => item?.label || "",
        loadOptions,
        name,
        onChange,
        options,
        value,
    } = props;

    const isAsync = !options && !!loadOptions;
    const layerDefaultOptions = useGetLayerOptions(isApple && isMobile);
    const [inputItems, setInputItems] = useState(options ?? []);
    const [isLoading, setIsLoading] = useState(false);
    const asyncLoadingCounter = useRef(0);

    const handleAsyncLoading = useCallback(
        async (loadingVersion: number, val?: string): Promise<[Option[], number]> => {
            let data: Option[] = [];
            if (loadOptions && val) {
                data = (await loadOptions(val)) ?? [];
            }

            return [data, loadingVersion];
        },
        [loadOptions],
    );

    const {
        getComboboxProps,
        getInputProps,
        getItemProps,
        getMenuProps,
        getToggleButtonProps,
        highlightedIndex,
        isOpen,
        selectedItem,
        setInputValue,
    } = useDownshiftCombobox({
        items: inputItems,
        id: name,
        onInputValueChange: async ({ inputValue: val }) => {
            if (isAsync) {
                setIsLoading(true);
                asyncLoadingCounter.current++;
                const [loadedOptions, loadingVersion] = await handleAsyncLoading(asyncLoadingCounter.current, val);
                if (asyncLoadingCounter.current === loadingVersion) {
                    setInputItems(loadedOptions);
                    setIsLoading(false);
                }
            } else {
                if (val?.length && val !== selectedItem?.label) {
                    setInputItems(filterOptions(val, options, alwaysVisibleOptionsIds));
                } else {
                    setInputItems(options ?? []);
                }
            }
        },
        onStateChange: changes => {
            if (
                changes.type === useDownshiftCombobox.stateChangeTypes.InputChange &&
                !changes.inputValue &&
                selectedItem
            ) {
                if (allowDeselect) {
                    onChange(null);
                }
                // TODO: setTimeout is hack
                setTimeout(() => {
                    setInputValue(changes.inputValue ?? "");
                }, 0);
            } else if (changes.type === useDownshiftCombobox.stateChangeTypes.ToggleButtonClick) {
                setInputItems(options ?? []);
            }
        },
        onSelectedItemChange: changes => {
            onChange(changes.selectedItem ?? null);
        },
        onIsOpenChange: changes => {
            if (!changes.isOpen && changes.type === useDownshiftCombobox.stateChangeTypes.InputBlur) {
                setInputValue(itemToString(selectedItem));
            }
        },
        initialSelectedItem: !isAsync ? options?.find(val => val.id === defaultValue) : defaultValue,
        itemToString,
        selectedItem: (!isAsync ? options?.find(val => val.id === value) : value) ?? null,
    });

    const { renderLayer, layerProps, layerSide, triggerProps, triggerBounds } = useLayer({
        ...layerDefaultOptions,
        isOpen,
    });

    const getListProps = ({ style, ...rest }: UseComboboxGetMenuPropsOptions) =>
        getMenuProps({
            ...layerProps,
            style: { width: triggerBounds?.width, ...layerProps.style, ...(style ?? {}) },
            ...rest,
        });

    const getInputContainerProps = (triggerOptions?: HTMLAttributes<HTMLElement>) =>
        getToggleButtonProps({
            ...triggerProps,
            ...triggerOptions,
            disabled: disabled,
            onClick: event => {
                (
                    event as ReactMouseEvent<HTMLButtonElement, MouseEvent> & {
                        preventDownshiftDefault?: boolean;
                    }
                ).preventDownshiftDefault = isOpen;
            },
        });

    const renderList = isOpen ? renderLayer : () => <span hidden ref={getMenuProps().ref} />;

    return {
        getComboboxProps,
        getInputContainerProps,
        getInputProps,
        getItemProps,
        getListProps,
        highlightedIndex,
        inputItems,
        isLoading,
        isOpen,
        layerSide,
        renderList,
        selectedItem,
    };
};
