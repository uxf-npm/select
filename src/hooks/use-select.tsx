import { useSelect as UseDownshiftSelect, UseSelectGetMenuPropsOptions } from "downshift";
import React, { HTMLAttributes, MouseEvent as ReactMouseEvent } from "react";
import { useLayer } from "react-laag";
import { useGetLayerOptions } from "../utils/use-get-layer-options";

export type SelectOption<T extends any = number | string> = {
    key?: number | string;
    id: T;
    label: string;
    disabled?: boolean;
};

export type UseSelectProps<Option extends SelectOption> = {
    defaultValue?: number | string;
    disabled?: boolean;
    isMobile?: boolean;
    isApple?: boolean;
    itemToString?: (item?: Option | null) => string;
    name: string;
    onChange: (value: Option | null) => void;
    options: Option[];
    value?: number | string;
};

export const useSelect = <Option extends SelectOption>(props: UseSelectProps<Option>) => {
    const {
        defaultValue,
        disabled,
        isMobile,
        isApple,
        itemToString = item => item?.label || "",
        name,
        onChange,
        options,
        value,
    } = props;

    const layerDefaultOptions = useGetLayerOptions(isApple && isMobile);

    const { getItemProps, getMenuProps, getToggleButtonProps, highlightedIndex, isOpen, selectedItem } =
        UseDownshiftSelect({
            items: options,
            id: name,
            onSelectedItemChange: changes => {
                onChange(changes.selectedItem ?? null);
            },
            initialSelectedItem: options.find(val => val.id === defaultValue),
            itemToString,
            selectedItem: options.find(val => val.id === value) ?? null,
        });

    const { renderLayer, layerProps, layerSide, triggerProps, triggerBounds } = useLayer({
        ...layerDefaultOptions,
        isOpen,
    });

    const getListProps = ({ style, ...rest }: UseSelectGetMenuPropsOptions) =>
        getMenuProps({
            ...layerProps,
            style: { width: triggerBounds?.width, ...layerProps.style, ...(style ?? {}) },
            ...rest,
        });

    const getInputContainerProps = (triggerOptions?: HTMLAttributes<HTMLElement>) =>
        getToggleButtonProps({
            ...triggerProps,
            ...triggerOptions,
            disabled: disabled,
            onClick: event => {
                (
                    event as ReactMouseEvent<HTMLButtonElement, MouseEvent> & {
                        preventDownshiftDefault?: boolean;
                    }
                ).preventDownshiftDefault = isOpen;
            },
        });

    const renderList = isOpen ? renderLayer : () => <span hidden ref={getMenuProps().ref} />;

    return {
        getInputContainerProps,
        getItemProps,
        getListProps,
        highlightedIndex,
        isOpen,
        layerSide,
        renderList,
        selectedItem,
    };
};
