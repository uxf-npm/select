import { removeAccents } from "./remove-accents";

type AutocompleteOption<T extends any = number | string> = {
    id: T;
    label: string;
};

export const filterOptions = <Option extends AutocompleteOption>(
    inputValue: string,
    options: Option[] = [],
    alwaysVisibleOptionsIds: Option["id"][] = [],
) => {
    if (inputValue === "") {
        return options;
    }

    const words = inputValue
        .replace(/[(]/, "")
        .replace(/[)]/, "")
        .replace(/[-]/, " ")
        .replace(/[+]/, " ")
        .replace(/[.]/, " ")
        .split(" ")
        .map(w => removeAccents(w).trim())
        .filter(v => v !== "");

    const filteredOptions = options.filter(option => {
        if (alwaysVisibleOptionsIds.includes(option.id)) {
            return false;
        }

        const optionLabel =
            "\0" +
            option.label
                .replace(/[(]/, "")
                .replace(/[)]/, "")
                .replace(/[-]/, " ")
                .replace(/[+]/, " ")
                .replace(/[.]/, " ")
                .split(" ")
                .map(w => removeAccents(w).trim())
                .join("\0");

        return words.every(word => new RegExp(`\0${word}`, "i").test(optionLabel));
    });

    const alwaysVisibleOptions = options.filter(option => alwaysVisibleOptionsIds.includes(option.id));

    return [...filteredOptions, ...alwaysVisibleOptions];
};
