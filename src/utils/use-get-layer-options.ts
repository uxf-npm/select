import { useMemo } from "react";
import { Options } from "react-laag/dist/types";
import ResizeObserver from "resize-observer-polyfill";

const LAYER_DEFAULT_OPTIONS: Partial<Options> = {
    placement: "bottom-start",
    possiblePlacements: ["bottom-start", "top-start"],
    ResizeObserver,
    triggerOffset: 0,
};

const LAYER_DEFAULT_OPTIONS_APPLE: Partial<Options> = {
    ...LAYER_DEFAULT_OPTIONS,
    auto: true,
    overflowContainer: false,
    snap: false,
};

export function useGetLayerOptions(appleOptions?: boolean): Partial<Options> {
    return useMemo(() => (appleOptions ? LAYER_DEFAULT_OPTIONS_APPLE : LAYER_DEFAULT_OPTIONS), [appleOptions]);
}
