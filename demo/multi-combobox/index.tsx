import React, { FC, HTMLAttributes, useCallback, useState } from "react";
import { MultiComboboxOption, useMultiCombobox } from "../../src/hooks/use-multi-combobox";

const options = [
    { id: 1, label: "One" },
    { id: 2, label: "Two" },
    { id: 3, label: "Three" },
];

export const MultiCombobox: FC<HTMLAttributes<HTMLDivElement>> = () => {
    const [state, setState] = useState<MultiComboboxOption<number>[] | null>(null);

    const name = "multi-combobox-1";
    const onChange = useCallback(a => setState(a), []);

    const {
        inputItems,
        getComboboxProps,
        getInputContainerProps,
        getInputProps,
        getItemProps,
        getListProps,
        renderList,
        selectedItems,
        getSelectedItemProps,
        getDropdownProps,
    } = useMultiCombobox<MultiComboboxOption<number>>({
        name,
        options,
        value: state?.map(i => i.id),
        onChange,
    });

    return (
        <>
            <div {...getComboboxProps()}>
                <div {...getInputContainerProps()}>
                    <input {...getInputProps({ ...getDropdownProps({ name }) })} />
                </div>
            </div>
            {renderList(
                <div {...getListProps({ style: { backgroundColor: "white" } })}>
                    {inputItems.length > 0 ? (
                        inputItems.map((item, index) => {
                            const itemProps = getItemProps({
                                item,
                                index,
                            });

                            return (
                                <div key={item.key || item.id} {...itemProps}>
                                    <span>{item.label}</span>
                                </div>
                            );
                        })
                    ) : (
                        <>no options</>
                    )}
                </div>,
            )}
            {selectedItems.length > 0 && (
                <div>
                    {selectedItems.map((selectedItem, index) => (
                        <span
                            key={`selected-item-${index}`}
                            {...getSelectedItemProps({
                                selectedItem,
                                index,
                            })}
                        >
                            {selectedItem.label}
                        </span>
                    ))}
                </div>
            )}
        </>
    );
};
