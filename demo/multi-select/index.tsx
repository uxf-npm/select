import React, { FC, HTMLAttributes, useCallback, useState } from "react";
import { useMultiSelect, MultiSelectOption } from "../../src/hooks/use-multi-select";

const options: MultiSelectOption<number>[] = [
    { id: 1, label: "One" },
    { id: 2, label: "Two" },
    { id: 3, label: "Three" },
];

export const MultiSelect: FC<HTMLAttributes<HTMLDivElement>> = () => {
    const [state, setState] = useState<MultiSelectOption<number>[] | null>(null);

    const onChange = useCallback(a => setState(a), []);

    const {
        getInputContainerProps,
        getItemProps,
        getListProps,
        renderList,
        selectedItems,
        getSelectedItemProps,
        getDropdownProps,
        removeSelectedItem,
    } = useMultiSelect<MultiSelectOption<number>>({
        name: "multi-select-1",
        options,
        value: state?.map(i => i.id),
        onChange,
    });

    return (
        <>
            <div {...getInputContainerProps()}>
                <span {...getDropdownProps()}>label {selectedItems.length + " items selected"}</span>
            </div>
            {renderList(
                <div {...getListProps({})}>
                    {options.length > 0 ? (
                        options.map((item, index) => {
                            const itemProps = getItemProps({
                                item,
                                index,
                            });

                            return (
                                <div key={item.key || item.id} {...itemProps}>
                                    <span>{item.label}</span>
                                </div>
                            );
                        })
                    ) : (
                        <>no options</>
                    )}
                </div>,
            )}
            {selectedItems.length > 0 && (
                <div>
                    {selectedItems.map((selectedItem, index) => (
                        <span
                            key={`selected-item-${index}`}
                            {...getSelectedItemProps({
                                selectedItem,
                                index,
                                onClick: () => removeSelectedItem(selectedItem),
                            })}
                        >
                            {selectedItem.label}
                        </span>
                    ))}
                </div>
            )}
        </>
    );
};
