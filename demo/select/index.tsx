import React, { FC, HTMLAttributes, useCallback, useState } from "react";
import { SelectOption, useSelect } from "../../src/hooks/use-select";

const options: SelectOption<number>[] = [
    { id: 1, label: "One" },
    { id: 2, label: "Two" },
    { id: 3, label: "Three" },
];

export const Select: FC<HTMLAttributes<HTMLDivElement>> = () => {
    const [state, setState] = useState<SelectOption<number> | null>(null);

    const onChange = useCallback(a => setState(a), []);

    const { getInputContainerProps, getItemProps, getListProps, renderList, selectedItem } = useSelect<
        SelectOption<number>
    >({
        name: "select-1",
        value: state?.id,
        onChange,
        options,
    });

    return (
        <>
            <div {...getInputContainerProps()}>
                label <span>{selectedItem?.label || "Select something"}</span>
            </div>
            {renderList(
                <div {...getListProps({ style: { backgroundColor: "white" } })}>
                    {options.length > 0 ? (
                        options.map((item, index) => {
                            const itemProps = getItemProps({
                                item,
                                index,
                            });

                            return (
                                <div key={item.key || item.id} {...itemProps}>
                                    <span>{item.label}</span>
                                </div>
                            );
                        })
                    ) : (
                        <div>no options</div>
                    )}
                </div>,
            )}
        </>
    );
};
