import React, { FC, HTMLAttributes, useCallback, useState } from "react";
import { ComboboxOption, useCombobox } from "../../src/hooks/use-combobox";

const options = [
    { id: 1, label: "One" },
    { id: 2, label: "Two" },
    { id: 3, label: "Three" },
];

export const Combobox: FC<HTMLAttributes<HTMLDivElement>> = () => {
    const [state, setState] = useState<ComboboxOption<number> | null>(null);

    const name = "combobox-1";
    const onChange = useCallback(a => setState(a), []);

    const {
        inputItems,
        getComboboxProps,
        getInputContainerProps,
        getInputProps,
        getItemProps,
        getListProps,
        renderList,
    } = useCombobox<ComboboxOption<number>>({
        name,
        options,
        value: state?.id,
        onChange,
    });

    return (
        <>
            <div {...getComboboxProps()}>
                <div {...getInputContainerProps()}>
                    <input
                        {...getInputProps({
                            name,
                        })}
                    />
                </div>
            </div>
            {renderList(
                <div {...getListProps({ style: { backgroundColor: "white" } })}>
                    {inputItems.length > 0 ? (
                        inputItems.map((item, index) => {
                            const itemProps = getItemProps({
                                item,
                                index,
                            });

                            return (
                                <div key={item.key || item.id} {...itemProps}>
                                    <span>{item.label}</span>
                                </div>
                            );
                        })
                    ) : (
                        <div>no options</div>
                    )}
                </div>,
            )}
        </>
    );
};
