import React from "react";
import ReactDOM from "react-dom";
import { Combobox } from "./combobox";
import { MultiCombobox } from "./multi-combobox";
import { MultiSelect } from "./multi-select";
import { Select } from "./select";

export default class App extends React.Component {
    render() {
        return (
            <>
                <div className="mb">
                    Combobox:
                    <Combobox />
                </div>
                <div className="mb">
                    MultiCombobox:
                    <MultiCombobox />
                </div>
                <div className="mb">
                    Select:
                    <Select />
                </div>
                <div className="mb">
                    MultiSelect:
                    <MultiSelect />
                </div>
            </>
        );
    }
}

const domContainer = document.querySelector("#app");
ReactDOM.render(<App />, domContainer);
